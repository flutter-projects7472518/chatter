import 'package:flutter/material.dart';

const users = [
  userBeh,
  userGordon,
  userSalvatore,
  userSacha,
  userDeven,
  userSahil,
  userReuben,
  userNash,
];

const userBeh = DemoUser(
  id: 'beh',
  name: 'C. Beh Ardieu ALOTIN',
  image: 'https://b-folio.000webhostapp.com/img/beh.jpg',
);

const userGordon = DemoUser(
  id: 'gordon',
  name: 'Gordon Hayses',
  image:
      'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
);

const userSalvatore = DemoUser(
  id: 'salvatore',
  name: 'Salvatore Giordano',
  image:
      'https://pbs.twimg.com/profile_images/1252869649349238787/cKVPSIyG_400x400.jpg',
);

const userSacha = DemoUser(
  id: 'sacha',
  name: 'Sacha Arbonel',
  image:
      'https://images.pexels.com/photos/3779853/pexels-photo-3779853.png?auto=compress&cs=tinysrgb&w=400',
);

const userDeven = DemoUser(
  id: 'deven',
  name: 'Deven Joshi',
  image:
      'https://images.pexels.com/photos/2884842/pexels-photo-2884842.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
);

const userSahil = DemoUser(
  id: 'sahil',
  name: 'Sahil Kumar',
  image:
      'https://images.pexels.com/photos/17086220/pexels-photo-17086220/free-photo-of-homme-jeune-style-moderne.jpeg?auto=compress&cs=tinysrgb&w=400',
);

const userReuben = DemoUser(
  id: 'reuben',
  name: 'Reuben Turner',
  image:
      'https://pbs.twimg.com/profile_images/1370571324578480130/UxBBI30i_400x400.jpg',
);

const userNash = DemoUser(
  id: 'nash',
  name: 'Nash Ramdial',
  image:
      'https://img.freepik.com/photos-gratuite/roi-lion-armure-complete-arme-generative-ai_191095-524.jpg?size=626&ext=jpg&ga=GA1.2.1182823721.1690241980&semt=sph',
);

@immutable
class DemoUser {
  final String id;
  final String name;
  final String image;

  const DemoUser({
    required this.id,
    required this.name,
    required this.image,
  });
}
